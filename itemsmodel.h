#ifndef ITEMSMODEL_H
#define ITEMSMODEL_H

#include <QAbstractListModel>
#include <QVector>

class SingleItem;
class SpellCheckService;
class QQuickTextDocument;

class ItemsModel : public QAbstractListModel
{
    Q_OBJECT
public:
    ItemsModel(SpellCheckService *spellCheckService);
    virtual ~ItemsModel();

public:
    virtual int rowCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

public:
    Q_INVOKABLE void submitToSpellCheck() const;
    Q_INVOKABLE void initHighlighter(int index, QQuickTextDocument *document);

private:
    SpellCheckService *m_SpellCheckService;
    QVector<SingleItem *> m_ItemsList;
};

#endif // ITEMSMODEL_H
