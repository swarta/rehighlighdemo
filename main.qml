import QtQuick 2.2
import QtQuick.Dialogs 1.1
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1
import QtQuick.Layouts 1.1

ApplicationWindow {
    id: mainWindow
    title: "RehighlightTest"
    width: 500
    height: 400
    visible: true

    ColumnLayout {
        anchors.fill: parent

        Button {
            text: "Check spelling"
            width: 100
            height: 30
            onClicked: itemsModel.submitToSpellCheck()
        }

        ListView {
            Layout.fillHeight: true
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 5
            model: itemsModel

            delegate: Rectangle {
                color: "#eeeeee"
                width: parent.width
                height: 50

                Rectangle {
                    id: descriptionRect
                    height: 30
                    width: parent.width - 20
                    border.width: captionTextEdit.activeFocus ? 1 : 0
                    border.color: "black"
                    clip: true

                    Flickable {
                        id: wrapperFlick
                        contentWidth: captionTextEdit.paintedWidth
                        contentHeight: captionTextEdit.paintedHeight
                        anchors.fill: parent
                        interactive: false
                        flickableDirection: Flickable.HorizontalFlick
                        height: 30
                        clip: true
                        focus: false

                        function ensureVisible(r) {
                            if (contentX >= r.x)
                                contentX = r.x;
                            else if (contentX+width <= r.x+r.width)
                                contentX = r.x+r.width-width;
                        }

                        TextEdit {
                            id: captionTextEdit
                            width: wrapperFlick.width
                            height: wrapperFlick.height
                            text: display
                            readOnly: true

                            Component.onCompleted: {
                                itemsModel.initHighlighter(index, captionTextEdit.textDocument)
                            }

                            onCursorRectangleChanged: wrapperFlick.ensureVisible(cursorRectangle)
                        }
                    }
                }
            }
        }
    }
}
