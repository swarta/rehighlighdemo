#ifndef SINGLEITEM_H
#define SINGLEITEM_H

#include <QObject>
#include <QString>
#include <QSet>

class SingleItem : public QObject
{
    Q_OBJECT
public:
    SingleItem(const QString &caption) :
        m_Caption(caption)
    { }

public:
    const QString &getCaption() const { return m_Caption; }
    void setCaption(const QString &caption) { m_Caption = caption; }
    const QSet<QString> &getErrors() const { return m_ErrorsInCaption; }

    void setErrors(const QSet<QString> &errors) {
        m_ErrorsInCaption.clear();
        m_ErrorsInCaption.unite(errors);

        emit spellCheckResultsReady();
    }

signals:
    void spellCheckResultsReady();

private:
    QString m_Caption;
    QSet<QString> m_ErrorsInCaption;
};

#endif // SINGLEITEM_H
