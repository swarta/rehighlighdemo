#ifndef SPELLCHECKSERVICE_H
#define SPELLCHECKSERVICE_H

#include <QVector>

class SpellCheckWorker;
class SingleItem;

class SpellCheckService
{
public:
    SpellCheckService();

public:
    void startChecking();
    void submitItems(const QVector<SingleItem*> &items);

private:
    SpellCheckWorker *m_SpellCheckWorker;
};

#endif // SPELLCHECKSERVICE_H
