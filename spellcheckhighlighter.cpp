#include <QDebug>
#include <QStringList>
#include "spellcheckhighlighter.h"
#include "singleitem.h"

SpellCheckHighlighter::SpellCheckHighlighter(QTextDocument *document, SingleItem *item) :
    QSyntaxHighlighter(document),
    m_ItemToCheck(item)
{
}

void SpellCheckHighlighter::highlightBlock(const QString &text) {
    QSet<QString> errors = m_ItemToCheck->getErrors();
    qDebug() << "Highlighting block with errors:" << QStringList::fromSet(errors);

    int i = 0;
    int size = text.size();
    int lastStart = -1;

    QColor errorColor = QColor::fromRgb(200, 10, 10);

    while (i < size) {
        QChar c = text[i];
        if (c == QChar::Space || c == QChar::Tabulation || c == QChar::CarriageReturn) {
            if (lastStart != -1) {
                int wordLength = i - lastStart;
                QString word = text.mid(lastStart, wordLength);
                if (errors.contains(word)) {
                    qDebug() << "setting format on position" << lastStart << "and length" << wordLength;
                    setFormat(lastStart, wordLength, errorColor);
                }

                lastStart = -1;
            }
        } else {
            if (lastStart == -1) {
                lastStart = i;
            }
        }

        i++;
    }

    if (lastStart != -1) {
        int wordLength = size - lastStart;
        QString word = text.mid(lastStart, wordLength);
        if (errors.contains(word)) {
            qDebug() << "setting format on position" << lastStart << "and length" << wordLength;
            setFormat(lastStart, wordLength, errorColor);
        }
    }
}
